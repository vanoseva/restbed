### What is this repository for? ###

Restbed is a C++ framework that enables you to embed RESTful services within your application. Restbed is designed
to get out of your way; it enables you to bring RESTful functionality to your product without imposing design 
decisions upon you.

### How do I get set up? ###

Please see the [wiki](https://bitbucket.org/Corvusoft/restbed/wiki/Home) for setup and contribution guidelines.

### Who do I talk to? ###

Please submit all enhancements, proposals, and issues via the [issue tracker](https://bitbucket.org/Corvusoft/restbed/issues/new).
